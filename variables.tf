variable "aws_profile" {}
variable "aws_region" {}
variable "duckdns_domain" {}
variable "duckdns_token" {}
variable "environment" {}
variable "key_name" {}
variable "private_key_path" {}
variable "prod_instance_type" {}
variable "public_key_path" {}
variable "splunk_sim_realm" {}
variable "splunk_sim_token" {}
variable "user" {}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]
  name_regex = "^amzn2-ami-hvm-.*x86_64-gp2$"
}

data "template_file" "user_data" {
    template = file("./k3s.yaml")
    vars = {
        duckdns_domain = var.duckdns_domain
        duckdns_token = var.duckdns_token
        environment = var.environment
        splunk_sim_realm = var.splunk_sim_realm
        splunk_sim_token = var.splunk_sim_token
        user = var.user
    }
}