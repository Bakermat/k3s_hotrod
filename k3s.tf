terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

resource "aws_key_pair" "ec2" {
  key_name = var.key_name
  public_key = file(var.public_key_path)
}

resource "aws_security_group" "sg_k3s_hotrod" {
  name        = "K3s w/ HOTROD"
  description = "Allow K3s inbound traffic for HOTROD"

  ingress {
    description = "HOTROD web app"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_spot_instance_request" "aws_linux" {
  instance_type = var.prod_instance_type
  wait_for_fulfillment = true
  ami = data.aws_ami.amazon-linux-2.id
  key_name = aws_key_pair.ec2.id
  vpc_security_group_ids = [aws_security_group.sg_k3s_hotrod.id]
  user_data = data.template_file.user_data.rendered

  tags = {
    Name = "hotrod-1"
  }
}

output "instance_id" {
  value = aws_spot_instance_request.aws_linux.spot_instance_id
}

output "public_ip" {
  value = aws_spot_instance_request.aws_linux.public_ip
}

output "ssh_login" {
    value = "ssh ec2-user@${aws_spot_instance_request.aws_linux.public_ip} -i ${var.private_key_path}"
}